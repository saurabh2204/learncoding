package com.saurabh;

import java.util.*;

public class PrimeNumber {

    public static void main(String args[]) {
        Scanner sc1 = new Scanner(System.in);
        int num = sc1.nextInt();
        if (findIfPrime(num))
            System.out.println("EnteredNumber is Prime");
        else
            System.out.println("Not Prime");
    }

    private static boolean findIfPrime(int a) {
        boolean flag = false;
        for (int i = 2; i < a / 2; i++) {
            if (a % i != 0) {
                flag = true;
                break;

            }
        }
         if (flag){
             return true;
         }
         else  return  false;
    }
}