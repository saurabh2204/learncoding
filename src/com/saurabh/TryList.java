package com.saurabh;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
public class TryList{
    public static void main(String[] args){

        ArrayList<Integer> list = new ArrayList<Integer>();
        //Instantiating an ArrayList object
        list.add(1001);
        list.add(2015);
        list.add(4566);
        list.add(90012);
        list.add(100);
        list.add(2144);
        list.add(43);
        list.add(2345);
        list.add(785);
        list.add(6665);
        list.add(6435);
        list.add(6432);
        System.out.println("Contents of the Array List: \n"+list);
        //Sorting the array list
        Collections.sort(list);
        System.out.println("Minimum value: "+list.get(0));
        System.out.println("Maximum value: "+list.get(list.size()-1));
        List<String> list4 = new ArrayList<String>();

    }
}