package com.saurabh;

import java.util.Scanner;

public class ControlStatements {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Student Number");
        int num = sc.nextInt();
/*

        if (num > 100) {
            System.out.println("Invalid marks, please enter marks less or equal to than 100");
        }

        if (num <= 100) {
            System.out.println("*****Congratulations******");
        } else {
            System.out.println("Valid Number entered");
        }
*/

        if (num == 100) {
            System.out.println("Greaat! Keep it up..score is 100");
        } else if (num > 75 && num < 100) {
            System.out.println("Excellent! Next time hit harder");
        } else {
            System.out.println("Sorry! take one more free GCP certification attempt");
        }

        String whatToPrint = (num > 75 && num <= 100) ? "Awesome!keep up great work" : "Please re-try with one more free attempt";
        System.out.println(whatToPrint);


    }
}
