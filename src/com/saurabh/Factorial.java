package com.saurabh;

public class Factorial {
    public static void main (String args[]){
/*
            int fac1 =5;
            int fact1 =1;
            for (int i =1 ; i<= fac1; i++){
                fact1 = fact1 * i;

            }
            System.out.println(fact1);

//        Solution 2:
            int fac2 =6;
            int fact2 =1;
            for (int i =fac2; i > 0; i--){
                fact2 = fact2*i;

            }
*/

            // Solution 3 - Recursion
        int fac3 =6;
       int    fact3= findFact(fac3);

        System.out.println(fact3);

    }

    private static int findFact(int fac3) {
        if (fac3 >=1)
             return    (fac3*findFact(fac3-1));
        else return 1;
    }


}
