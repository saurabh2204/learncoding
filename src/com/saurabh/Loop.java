package com.saurabh;

import java.util.Scanner;

public class Loop {

    public static void main (String [] a)
    {
        // for loop
        for (int i=0; i<10; i++)
        {
            System.out.println("printing from for loop"+i);
        }


        //while loop for table
        System.out.println("Please enter number for which you want to see table  ---- using while loop");
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        int i =1;
        while ( i <= 10){
            System.out.println(num+"*"+i+" = "+i*num);
            i++;
        }
        System.out.println("final i value"+i);


        // do while loop
        int j=0;
        System.out.println("Enter the number for table -- using do while");
        int k= sc.nextInt();
        do {
            System.out.println (k+"*"+j+" = "+k*j);
            j++;
        }while (j<11);



        // for each loop

        int ar[] = {2,9,6,0,5,7};
        for (int x : ar)
        {
       if   (x%2 ==0)
           System.out.println(x);
        }

    }
}
